module LWIA
using DataFrames, Measurements, Dates, CSV, Statistics, LinearAlgebra, LsqFit
import Measurements.value, Measurements.uncertainty

export Isotope, IsotopicRatio, DHr, O17O16r, O18O16r, internal_name, by_internal_name, Vial, Reading, Batch, MeasurementSeries, parse_measurement_series, get_readings_of_vial, get_batches_of_vial, readings_to_dataframe

### Isotope related stuff

struct Isotope
    name::String
    short_name::String
end

Base.show(io::IO, x::Isotope) = print(io, "Isotope($(x.name))")

struct IsotopicRatio
    common_isotope::Isotope
    rare_isotope::Isotope
end

const DHr = IsotopicRatio(Isotope("Protium", "H"), Isotope("Deuterium", "D"))
const O17O16r = IsotopicRatio(Isotope("Oxygen-16", "O16"), Isotope("Oxygen-17", "O17"))
const O18O16r = IsotopicRatio(Isotope("Oxygen-16", "O16"), Isotope("Oxygen-18", "O18"))

Base.show(io::IO, x::IsotopicRatio) = print(io, "R($(x.rare_isotope.short_name)/$(x.common_isotope.short_name))")

function internal_name(ratio::IsotopicRatio)::String
    return "$(ratio.rare_isotope.short_name)$(ratio.common_isotope.short_name)r"
end

function by_internal_name(name::String, ratios::Vector{IsotopicRatio})::IsotopicRatio
    for ratio in ratios
        if name == internal_name(ratio)
            return ratio
        end
    end
    return missing
end

### Data structures for a measurement process

# Vial on a tray whose isotopic composition is being measured
struct Vial
    name::String
    tray_pos::Tuple{Integer, Integer}
end

#Base.show(io::IO, x::Vial) = print(io, "$(x.name) ($(x.tray_pos[1]), $(x.tray_pos[2]))")
Base.show(io::IO, x::Vial) = print(io, x.name)

@enum ReadingFlag norm=1 abnormal_pressure=2 abnormal_density=3

# The data obtained from a single water injection
struct Reading
    time::DateTime
    pressure::Measurement # Gas pressure in Pascal
    temperature::Measurement # Gas temperature in Kelvin
    flag::ReadingFlag # flag given by the TIWA-45EP

    # R = (abundance of heavy isotope) / (abundance of light isotope)
    ratios::Dict{IsotopicRatio, Measurement}
end

# The repeated measurement of a single vial
struct Batch
    vial::Vial
    readings::Vector{Reading}
end

Base.show(io::IO, batch::Batch) = print(io, "Batch($(batch.vial), $(length(batch.readings)) readings)")

# A series of measurements that typically contains the repeated measurement of multiple vials
struct MeasurementSeries
    filename::String
    vials::Vector{Vial}
    measured_ratios::Vector{IsotopicRatio}
    
    # DataFrame containing columns:
    # injection_no::Integer, vial::Vial, reading::Reading
    data::DataFrame
    batches::Vector{Batch}
    max_batch_length::Int
end

Base.show(io::IO, x::MeasurementSeries) = print(io, "MeasurementSeries($(length(x.vials)) vials, $(nrow(x.data)) readings, $(length(x.batches)) batches)")

function parse_measurement_series(filename::String; discard::Union{Nothing, BitVector} = nothing)::MeasurementSeries
    raw_df = CSV.read(filename, DataFrame, delim = ",", skipto=3, header=2)

    # Hardcoded for the moment
    measured_ratios = IsotopicRatio[DHr, O17O16r, O18O16r]

    data = DataFrame(:injection_no => Integer[], :vial => Vial[], :reading => Reading[])

    vials = Vial[]
    batches = Batch[]
    max_batch_length = 0

    prev_vial = nothing
    cur_batch_readings = Reading[]
    
    for (i, row) in enumerate(eachrow(raw_df))
        time = DateTime(row["Time"], dateformat"mm/dd/yyyyHH:MM:SS.s")
        pressure_torr = row["GasP_torr"] ± row["GasP_torr_sd"]
        pressure = pressure_torr * 133.3223684 # Pa / torr

        temperature_celsius = row["GasT_C"] ± row["GasT_C_sd"]
        temperature = temperature_celsius + 273.15

        vial_pos::Tuple{Int, Int} = Tuple(parse.(Int, split(row["TrayPos"], "-")))
        vial = Vial(row["VialName"], vial_pos)

        if !(vial in vials)
            push!(vials, vial)
        end

        ratios::Dict{IsotopicRatio, Measurement} = Dict()
        for ratio in measured_ratios
        name = internal_name(ratio)
            value = row[name] ± row["$(name)_sd"]
            ratios[ratio] = value
        end

        flag = missing
        if row["Flag"] == "norm"
            flag = norm
        elseif row["Flag"] == "pres"
            flag = abnormal_pressure
        elseif row["Flag"] == "dens"
            flag = abnormal_density
        end

        reading = Reading(time, pressure, temperature, flag, ratios)

        # Filtering
        if discard != nothing
            if discard[i]
                continue
            end
        end


        if vial == prev_vial || prev_vial == nothing
            prev_vial = vial
            push!(cur_batch_readings, reading)
        else
            batch = Batch(prev_vial, cur_batch_readings)
            push!(batches, batch)
            if length(batch.readings) > max_batch_length
                max_batch_length = length(batch.readings)
            end
            cur_batch_readings = Reading[reading]
            prev_vial = vial
        end

        push!(data, Dict(
            :injection_no => row["InjectionNo"],
            :vial => vial,
            :reading => reading
        ))

        nbatch = sum(map(batch -> length(batch.readings), batches))
    end

    # Insert last batch
    batch = Batch(prev_vial, cur_batch_readings)
    push!(batches, batch)
    if length(batch.readings) > max_batch_length
        max_batch_length = length(batch.readings)
    end

    return MeasurementSeries(filename, vials, measured_ratios, data, batches, max_batch_length)
end

function get_vial(ms::MeasurementSeries, name::String)::Union{Vial, Nothing}
    for vial in ms.vials
        if vial.name == name
            return vial
        end
    end
    return nothing
end

function get_readings_of_vial(series::MeasurementSeries, sought_vial::Vial)::Vector{Reading}
    readings = Reading[]
    for (n, vial, reading) in eachrow(series.data)
        if vial == sought_vial
            push!(readings, reading)
        end
    end
    return readings
end

function get_batches_of_vial(series::MeasurementSeries, sought_vial::Vial)::Vector{Batch}
    return filter(x -> x.vial == sought_vial, series.batches)
end

function readings_to_dataframe(series::MeasurementSeries, readings::Vector{Reading})
    N = length(readings)
    df = DataFrame(
        :n => 1:N,
        :time => getproperty.(readings, :time),
        :pressure => getproperty.(readings, :pressure),
        :temperature => getproperty.(readings, :temperature),
        :flag => getproperty.(readings, :flag)
    )

    for ratio in series.measured_ratios
        df[!, "$(internal_name(ratio))"] = map(x -> x.ratios[ratio], readings)
    end

    return df
end

function get_batch_pairs(series::MeasurementSeries)::Vector{Tuple{Batch, Batch}}
    pairs = Tuple{Batch, Batch}[]
    for i in 1:length(series.batches)-1
        push!(pairs, (series.batches[i], series.batches[i+1]))
    end
    return pairs
end


function mean_and_std_of_batch(ms::MeasurementSeries, batch::Batch, ratio::IsotopicRatio, discard_first_half::Bool)::Measurement
    df = readings_to_dataframe(ms, batch.readings)
    ratios = df[!, internal_name(ratio)]
    if discard_first_half
        n = length(ratios)
        ratios = ratios[n÷2:n]
    end
    μ = mean(ratios)
    σ = std(ratios)
    # Combine sigma from error propagation and variance in the bunch itself with each other:
    return μ.val ± sqrt(σ.val^2 + μ.err^2)
end

# Normalize the isotopic ratios such that the mean of the first batch lies at 1 and the second one at 0.
# To ensure that the initial exponential falloff doesn't disturb the mean calculation, the second halves are used respectively.
# Returns a DataFrame with the normalized data and a measure of how much the two batches lie apart from each other.
function normalize_batch_pair(ms::MeasurementSeries, pair::Tuple{Batch, Batch}, normalize_by::IsotopicRatio)::Tuple{DataFrame, Float64}
    df1 = readings_to_dataframe(ms, pair[1].readings)
    df2 = readings_to_dataframe(ms, pair[2].readings)

    ratios_1 = df1[!, internal_name(normalize_by)]
    ratios_2 = df2[!, internal_name(normalize_by)]

    μ1 = mean_and_std_of_batch(ms, pair[1], normalize_by, true)
    μ2 = mean_and_std_of_batch(ms, pair[2], normalize_by, true)
    
    normed_R_1 = (ratios_1 .- μ2) ./ (μ1 - μ2)
    normed_R_2 = (ratios_2 .- μ2) ./ (μ1 - μ2)
    append!(normed_R_1, normed_R_2)

    n = [-nrow(df1):-1; 0:nrow(df2)-1]
    append!(df1, df2)
    df1.normed_R = normed_R_1
    df1.n = n

    return df1, (μ1 - μ2).val / (μ1 - μ2).err
end

function average_transition_curve(ms::MeasurementSeries, normalize_by::IsotopicRatio, diff_threshold::AbstractFloat)::DataFrame
    ns = [-ms.max_batch_length-1:-1; 0:ms.max_batch_length-1]
    datapoints = Vector{Measurement}[]
    for n in ns
        push!(datapoints, Measurement[])
    end

    for pair in get_batch_pairs(ms)
        df, delta = normalize_batch_pair(ms, pair, normalize_by)
        if delta > diff_threshold
            for row in eachrow(df)
                push!(datapoints[findfirst(ns .== row.n)], row.normed_R)
            end
        end
    end

    ys = zeros(Measurement, size(ns))
    for (i, n) in enumerate(ns)
        # Combine error propagation and variation between data points
        ys[i] = mean(datapoints[i]).val ± sqrt(mean(datapoints[i]).err^2 + std(datapoints[i]).val^2)
    end
    return DataFrame(:n => ns, :y => ys)
end

function determine_decay_parameter(series::MeasurementSeries, ratio::IsotopicRatio, threshold::Float64)::Measurement
    total_df = DataFrame(n = Float64[], normed_R = Float64[])
    for pair in LWIA.get_batch_pairs(series)
            df, delta = LWIA.normalize_batch_pair(series, pair, ratio)
            if abs(delta) > threshold
                    append!(total_df, df[!, [:n, :normed_R]], promote=true)
            end
    end

    fit_df = filter(x -> x.n >= 0, total_df)
    @. fitfunc(x, p) = p[2] * exp(-(p[1] * x))
    fit = curve_fit(fitfunc, fit_df.n, value.(fit_df.normed_R), uncertainty.(fit_df.normed_R), [1., 1.])
    return coef(fit)[1] ± stderror(fit)[1]
end

function batch_fitfunc(xs, p, decay_parameter)
    ys = zeros(length(xs))
    for (n, x) in enumerate(xs)
            if n > 1
                ys[n] = (p[x] + exp(-decay_parameter) * ys[n-1]) / (1 + exp(-decay_parameter))
            else
                # last parameter in p = first data point, because we don't know anything about the memory effect from before the measurement started
                ys[n] = p[length(p)]
            end
    end
    return ys
end

function fit_batches(ms::MeasurementSeries, ratio::IsotopicRatio, decay_parameter::Float64)::Tuple{Vector{Float64}, Vector{Float64}}
    batch_df = DataFrame(:batchno => Int[], :value => Measurement[])

    for (batchno, batch) in enumerate(ms.batches)
        for reading in batch.readings
            push!(batch_df, (batchno, reading.ratios[ratio]), promote = true)
        end
    end

    fit = curve_fit((x, p) -> batch_fitfunc(x, p, decay_parameter), batch_df.batchno, value.(batch_df.value), uncertainty.(batch_df.value), zeros(Float64, length(ms.batches) + 1))
    fit_errors = value.(batch_df.value) - batch_fitfunc(batch_df.batchno, coef(fit), decay_parameter)
    coefs = coef(fit)[1:length(ms.batches)]
    return coefs, fit_errors
end

function vial_average_of_coefs(ms::MeasurementSeries, ratio::IsotopicRatio, batch_coefs::Vector{Float64})::Dict{Vial, Measurement}
    vial_averages = Dict{Vial, Measurement}()
    for vial in ms.vials
        v_vals = Float64[]
        for (n, batch) in enumerate(ms.batches)
            if batch.vial == vial
                push!(v_vals, batch_coefs[n])
            end
        end
        vial_averages[vial] = mean(v_vals) ± std(v_vals)
    end
    return vial_averages
end

function batch_dev_from_fit(ms::MeasurementSeries, vial_averages::Dict{Vial, Measurement}, batch_coefs::Vector{Float64})::Vector{Measurement}
    devs = Measurement[]
    for (n, batch) in enumerate(ms.batches)
        dev = batch_coefs[n] - vial_averages[batch.vial]
        push!(devs, dev)
    end
    return devs
end

@. linfit(x, p) = p[1] * x + p[2]

function extract_vial_ratios(ms::MeasurementSeries; decay_batch_threshold::Float64 = 4.)::Tuple{Dict{Vial, Dict{IsotopicRatio, Measurement}}, Matrix}
    vial_ratios = Dict{Vial, Dict{IsotopicRatio, Measurement}}()
    for vial in ms.vials
        vial_ratios[vial] = Dict()
    end
    all_fit_errors = zeros(nrow(ms.data), length(ms.measured_ratios))
    for (i, ratio) in enumerate(ms.measured_ratios)
        decay_parameter = determine_decay_parameter(ms, ratio, decay_batch_threshold)
        #@show ratio, decay_parameter

        batch_coefs, fit_errors = fit_batches(ms, ratio, decay_parameter.val)
        all_fit_errors[:, i] = fit_errors
        vial_averages = vial_average_of_coefs(ms, ratio, batch_coefs)
        deviations = batch_dev_from_fit(ms, vial_averages, batch_coefs)

        # Drift fit
        N = 1:length(deviations)
        fit = curve_fit(linfit, N, value.(deviations), uncertainty.(deviations), [0., 0.])
        
        # Drift-corrected values
        batch_coefs_corrected = batch_coefs .- linfit(N, coef(fit))
        vial_averages_corrected = vial_average_of_coefs(ms, ratio, batch_coefs_corrected)
        for vial in ms.vials
            vial_ratios[vial][ratio] = vial_averages_corrected[vial]
        end
    end
    return vial_ratios, all_fit_errors
end

function load_standards(ms::MeasurementSeries, filename::String)::Dict{Vial, Dict{IsotopicRatio, Float64}}
    df = CSV.read(filename, DataFrame)
    standards = Dict()
    for row in eachrow(df)
        vial = get_vial(ms, String(row.StdName))
        if vial != nothing
            standards[vial] = Dict(DHr => row.delta2H / 1e3, O17O16r => row.deltaO17O18 / 1e3, O18O16r => row.deltaO18O16 / 1e3)
        end
    end
    return standards
end

# delta = (R - R_std) / R_std = R / R_std - 1
@. deltafit(x, p) = x / p[1] - 1

function calibrate_by_standards(ms::MeasurementSeries, standards::Dict{Vial, Dict{IsotopicRatio, Float64}}, vial_ratios::Dict{Vial, Dict{IsotopicRatio, Measurement}}, standard_vials::Vector{Vial})::Dict{IsotopicRatio, Measurement}
    standard_Rs = Dict{IsotopicRatio, Measurement}()
    for ratio in ms.measured_ratios
        deltas = [standards[std][ratio] for std in standard_vials]
        ratios = [vial_ratios[std][ratio] for std in standard_vials]
        fit = curve_fit(deltafit, value.(ratios), deltas, [2e-3, ])
        standard_Rs[ratio] = coef(fit)[1] ± stderror(fit)[1]
    end
    return standard_Rs
end

function deltas_from_calibration(ms::MeasurementSeries, vial_ratios::Dict{Vial, Dict{IsotopicRatio, Measurement}}, calibration::Dict{IsotopicRatio, Measurement})::Dict{Vial, Dict{IsotopicRatio, Measurement}}
    vial_deltas = Dict()
    for vial in ms.vials
        vial_deltas[vial] = Dict()
        for ratio in ms.measured_ratios
            vial_deltas[vial][ratio] = vial_ratios[vial][ratio] / calibration[ratio] - 1
        end
    end
    return vial_deltas
end

function extract_vial_deltas(filename_measurement::String, filename_standards::String, standards_names::Vector{String})::Dict{String, Dict{IsotopicRatio, Measurement}}
    series = parse_measurement_series(filename_measurement)
    vial_ratios, fit_errors = extract_vial_ratios(series)
    
    #@show fit_errors
    fit_errors_sigma = (fit_errors .- mean(fit_errors, dims=1)) ./ std(fit_errors, dims=1)
    fit_errors_norm = [LinearAlgebra.norm(fit_errors_sigma[i, :]) for i in 1:size(fit_errors_sigma)[1]]
    exclude = fit_errors_norm .> 3 * sqrt(3)

    # Re-conduct first steps
    series_corr = parse_measurement_series(filename_measurement, discard = exclude)
    vial_ratios_corr, _ = extract_vial_ratios(series_corr)

    standards = load_standards(series_corr, filename_standards)

    std_vials = [get_vial(series_corr, name) for name in standards_names]
    calibration = calibrate_by_standards(series_corr, standards, vial_ratios_corr, std_vials)
    vial_deltas = deltas_from_calibration(series_corr, vial_ratios_corr, calibration)
    vial_deltas_str = Dict()
    for (k, v) in vial_deltas
        vial_deltas_str[k.name] = v
    end
    return vial_deltas_str
end

function merge_deltas(delta_sets::Vector{Dict{String, Dict{IsotopicRatio, Measurement}}})::Dict{String, Dict{IsotopicRatio, Measurement}}
    deltas = Dict()
    vials = String[]
    ratios = IsotopicRatio[]
    for delta_set in delta_sets
        for vial in keys(delta_set)
            if !(vial in vials)
                push!(vials, vial)
            end
            for ratio in keys(delta_set[vial])
                if !(ratio in ratios)
                    push!(ratios, ratio)
                end
            end
        end
    end
    for vial in vials
        deltas[vial] = Dict()
        for ratio in ratios
            all_deltas = []
            for delta_set in delta_sets
                if haskey(delta_set, vial)
                    push!(all_deltas, delta_set[vial][ratio])
                end
            end
            μ = mean(all_deltas)
            if length(all_deltas) > 1
                σ = std(all_deltas)
            else
                σ = 0 ± 0
            end
            deltas[vial][ratio] = μ.val ± sqrt(μ.err^2 + σ.val^2)
        end
    end
    return deltas
end

# Module end
end
